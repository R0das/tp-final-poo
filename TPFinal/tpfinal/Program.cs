﻿using System;

namespace tpfinal
{
    class Program
    {
        static void Main(string[] args)
        {

            ClaseEmpleados jefe1 = new ClaseEmpleados("Roberto","Gines", 45, 'M', 65000, 23);
            ClaseArea area1 = new ClaseArea(jefe1, "Marketing");
            ClaseEmpleados jefe2 = new ClaseEmpleados("Juan", "Gonzales", 50, 'M', 72000, 25);
            ClaseArea area2 = new ClaseArea(jefe2, "Recursos");
            ClaseEmpleados jefe3 = new ClaseEmpleados("Emilia", "Suarez", 35, 'M', 34000, 11);
            ClaseArea area3 = new ClaseArea(jefe3, "Desarrollo");
            ClaseProcesador cpu = new ClaseProcesador(25000, "Intel i5", "Intel", 0, 6, 3);
            ClasePlacadeVideo gpu = new ClasePlacadeVideo(50000, "RTX 2070", "Nvidia", 20, 8, "HDMI");
            ClasePlacaMadre mother = new ClasePlacaMadre(9000, "H410m", "Gigabyte", 40, "DDR4", "B460");

            ClaseEmpresa Macrohard = new ClaseEmpresa(area1, area2, area3, "Macrohard", cpu, gpu, mother);
            //Creo la empresa

            bool salir = false;
            while (!salir){
                Console.Clear();
                int opcion;

                Console.WriteLine("Elija una opción del menú \n" +
                    "\n1) Contratar empleados" +
                    "\n2) Mostrar empleados por área" +
                    "\n3) Mostrar todos los empleados" +
                    "\n4) Aumentar salario de un empleado específico" +
                    "\n5) Aumentar salario de empleados por área" +
                    "\n6) Aumentar salario de todos los empleados" +
                    "\n7) Aumentar salario de un empleado por antiguedad" +
                    "\n8) Despedir empleado" +
                    "\n9) Aumentar stock de un producto específico por empresa" +
                    "\n10) Vender producto" +
                    "\n11) Calcular ganacias" +
                    "\n12) Salir\n");

                opcion = int.Parse(Console.ReadLine());

                switch (opcion)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Elija el área en el que quiere contratar un empleado.");
                        int area;
                        string aux = "";
                        int cont = 1;
                        foreach (ClaseArea a in Macrohard.lista_area)
                        {
                            aux += String.Format("\n {0}) {1}", cont, a.area_nombre);
                            cont += 1;
                        }
                        Console.WriteLine(aux);
                        area = int.Parse(Console.ReadLine());
                        if (area > Macrohard.lista_area.Count)
                        {
                            Console.WriteLine("El área ingresada no existe");
                            Console.ReadKey();
                            break;
                        }

                        Console.Clear();
                        int clase = 0;
                        Console.WriteLine("¿Que tipo de empleado quiere? \n" +
                            "\n1) Programador " +
                            "\n2) Administrador" +
                            "\n3) Diseñador\n");
                        clase = int.Parse(Console.ReadLine());

                        Console.Clear();
                        string nombre;
                        string apellido;
                        int edad;
                        char sexo;
                        int suel;
                        int anti;
                        Console.WriteLine("Por favor ingrese el nombre");
                        nombre = Console.ReadLine();
                        Console.WriteLine("Por favor ingrese el apellido");
                        apellido = Console.ReadLine();
                        Console.WriteLine("Por favor ingrese la edad");
                        edad = int.Parse(Console.ReadLine());
                        Console.WriteLine("Por favor ingrese el sexo(M o F)");
                        sexo = Console.ReadLine()[0];
                        Console.WriteLine("Por favor ingrese el sueldo del nuevo empleado.");
                        suel = int.Parse(Console.ReadLine());
                        Console.WriteLine("Por favor ingrese la antiguedad del nuevo empleado.");
                        anti = int.Parse(Console.ReadLine());

                        switch (clase)
                        {
                            case 1:
                                Console.Clear();
                                string len, exp;
                                Console.WriteLine("Programador");
                                Console.WriteLine("Ingrese el lenguaje de programación que domine.");
                                len = Console.ReadLine();
                                Console.WriteLine("Describa su experiencia laboral anterior.");
                                exp = Console.ReadLine();

                                Macrohard.lista_area[area - 1].contratarEmpleado(nombre, apellido, sexo, edad, suel, anti, len, exp);
                                Macrohard.lista_area[area - 1].empleadoTotal(nombre, apellido, edad, sexo, suel, anti);
                                Console.ReadKey();
                                break;
                            case 2:
                                Console.Clear();
                                bool lid, admin;
                                Console.WriteLine("Administrador");
                                Console.WriteLine("¿El empleado cuenta con un liderazgo propio?.(Responder si o no)");
                                string temp = Console.ReadLine();
                                if (temp.ToUpper() == "SI")
                                {
                                    lid = true;
                                }
                                else
                                {
                                    lid = false;
                                }
                                Console.WriteLine("¿El empleado cuenta con una buena administración del presupuesto?.(Responder si o no)");
                                string temp2 = Console.ReadLine();
                                if (temp2.ToUpper() == "SI")
                                {
                                    admin = true;
                                }
                                else
                                {
                                    admin = false;
                                }
                                Macrohard.lista_area[area - 1].contratarEmpleado(nombre, apellido, edad, sexo, suel, anti, lid, admin);
                                Macrohard.lista_area[area - 1].empleadoTotal(nombre, apellido, edad, sexo, suel, anti);
                                Console.ReadKey();
                                break;
                            case 3:
                                Console.Clear();
                                string tip, prg;
                                Console.WriteLine("Diseñador");
                                Console.WriteLine("Ingrese el tipo lenguaje que domine el empleado.");
                                tip = Console.ReadLine();
                                Console.WriteLine("Ingrese el programa que domine el nuevo empleado.");
                                prg = Console.ReadLine();

                                Macrohard.lista_area[area - 1].contratarEmpleado(nombre, apellido, sexo, edad, suel, anti, tip, prg);
                                Macrohard.lista_area[area - 1].empleadoTotal(nombre, apellido, edad, sexo, suel, anti);
                                Console.ReadKey();
                                break;
                            default:
                                Console.Clear();
                                Console.WriteLine("Ha ingresado una opción erronea, por favor vuelva a intentar e ingrese una opción correcta.");
                                Console.ReadKey();
                                break;

                        }
                        break;
                    case 2:
                        Console.Clear();
                        Console.WriteLine("Seleccione un área para ver a los empleados.");
                        string aux2 = "";
                        string Area = "";
                        int cont2 = 1;
                        foreach (ClaseArea a in Macrohard.lista_area)
                        {
                            aux2 += String.Format("\n {0}) {1}", cont2, a.area_nombre);
                            cont2 += 1;
                        }
                        Console.WriteLine(aux2);
                        int areas = int.Parse(Console.ReadLine());
                        if (areas > Macrohard.lista_area.Count)
                        {
                            Console.WriteLine("El área ingresada no existe");
                            Console.ReadKey();
                            break;
                        }
                        Console.Clear();
                        foreach (ClaseEmpleados a in Macrohard.lista_area[areas - 1].lista_emp)
                        {
                            Console.WriteLine(a.toString());
                        }

                        Console.ReadKey();
                        break;
                    case 3:
                        //mostrar todos los empleados
                        Console.Clear();
                        foreach(ClaseEmpleados a in area1.lista_emp)
                        {
                            Console.WriteLine(a.toString());
                        }

                        Console.ReadKey();
                        break;
                    case 4:

                        Console.ReadKey();
                        break;
                    case 5:
                        Console.Clear();
                        Console.WriteLine("¿De qué área es el empleado al que quiere aumentarle el suelo?.");
                        string aux3 = "";
                        int cont3 = 1;
                        foreach (ClaseArea a in Macrohard.lista_area)
                        {
                            aux3 += String.Format("\n {0}) {1}", cont3, a.area_nombre);
                            cont3 += 1;
                        }
                        Console.WriteLine(aux3);
                        int areas1 = int.Parse(Console.ReadLine());
                        if (areas1 > Macrohard.lista_area.Count)
                        {
                            Console.WriteLine("El área ingresada no existe");
                            Console.ReadKey();
                            break;
                        }


                        Console.ReadKey();
                        break;
                    case 6:
                        //aumentar salario a todos
                        Console.ReadKey();
                        break;
                    case 7:
                        //aumentar salario a empleados con x años

                        Console.WriteLine("Ingrese la cantidad mínima de años de servicio para aumentar el salario");
                        int años = int.Parse(Console.ReadLine());

                        Console.ReadKey();
                        break;
                    case 8:
                        //despedir empleados



                        Console.ReadKey();
                        break;
                    case 9:
                        Console.Clear();
                        int aumentar_stock;
                        Console.WriteLine("Los productos disponibles son:\n" +
                            "\n1)Procesador Intel i5" +
                            "\n2)RTX 2070" +
                            "\n3)H410m" +
                            "\n Eliga una opción.");
                        aumentar_stock = int.Parse(Console.ReadLine());
                        Console.Clear();
                        switch (aumentar_stock)
                        {
                        case 1:
                            int proc;
                            Console.WriteLine("¿Cuánto stock se desea agregar?");
                            proc = int.Parse(Console.ReadLine());
                            Macrohard.añadirStockCpu(proc);
                            Console.WriteLine("Se ha añadido el stock. Presione para salir.");

                            Console.ReadKey();
                            break;
                        case 2:
                            int vid;
                            Console.WriteLine("¿Cuánto stock se desea agregar?");
                            vid = int.Parse(Console.ReadLine());
                            Macrohard.añadirStockGpu(vid);
                            Console.WriteLine("Se ha añadido el stock. Presione para salir.");

                            Console.ReadKey();
                            break;
                        case 3:
                            int mot;
                            Console.WriteLine("¿Cuánto stock se desea agregar?");
                            mot = int.Parse(Console.ReadLine());
                            Macrohard.añadirStockCpu(mot);
                            Console.WriteLine("Se ha añadido el stock. Presione para salir.");

                            Console.ReadKey();
                            break;
                        default:
                            Console.WriteLine("Ha elegido una opción no válida. Vuelva a intentar con una opción correcta.");
                            Console.ReadKey();
                            break;
                        }
                        break;
                    case 10:
                        //Vender: Mostrar productos y stock. Restar un stock dependiendo de la compra. Si hay 0 stock, no dejar que se venda. Sumar el precio de compra a ganancia de la empresa.
                        Console.Clear();
                        int vender_prod;
                        int prodc_vendido;
                        int stock_proc = cpu.stock;
                        int stock_vid = gpu.stock;
                        int stock_mot = mother.stock;
                        Console.WriteLine("\n               Macrohard" +
                            "\n     Producto             Stock" +
                            "\n 1)Procesador Intel i5      {0}"  +
                            "\n 2)RTX 2070                 {1}"  +
                            "\n 3)H410m                    {2}"  +
                            "\n Eliga una opción.", stock_proc, stock_vid, stock_mot);
                        vender_prod = int.Parse(Console.ReadLine());
                        Console.Clear();
                        switch (vender_prod)
                        {
                            case 1:
                                if(stock_proc == 0)
                                {
                                    Console.WriteLine("El producto no cuenta con stock. Lamentamos las molestias");
                                }
                                else
                                {
                                    int compra = 0;
                                    Console.WriteLine("\nEl producto tiene un precio estimado de ${0}." +
                                    "\n¿Desea realizar la compra?" +
                                    "\n1) Si.              2)No.", cpu.valor);
                                    compra = int.Parse(Console.ReadLine());
                                    if (compra == 1)
                                    {
                                        cpu.stock -= 1;
                                        prodc_vendido = cpu.valor;
                                        Macrohard.añadir_monto(prodc_vendido);
                                        Console.WriteLine("Se ha concretado la compra. Muchas gracias por seleccionar nuestro servicio.");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Lamentamos que el producto no sea de su agrado. Por favor, revise los otros productos.");
                                    }
                                    
                                }
                                Console.ReadKey();
                                break;
                            case 2:
                                if (stock_vid == 0)
                                {
                                    Console.WriteLine("El producto no cuenta con stock. Lamentamos las molestias");
                                }
                                else
                                {
                                    int compra = 0;
                                    Console.WriteLine("\nEl producto tiene un precio estimado de ${0}." +
                                    "\n¿Desea realizar la compra?" +
                                    "\n1) Si.              2)No.", gpu.valor);
                                    compra = int.Parse(Console.ReadLine());
                                    if (compra == 1)
                                    {
                                        prodc_vendido = gpu.valor;
                                        Macrohard.añadir_monto(prodc_vendido);
                                        gpu.stock -= 1;
                                        Console.WriteLine("Se ha concretado la compra. Muchas gracias por seleccionar nuestro servicio.");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Lamentamos que el producto no sea de su agrado. Por favor, revise los otros productos.");
                                    }
                                    
                                }
                                Console.ReadKey();
                                break;
                            case 3:
                                if (stock_mot == 0)
                                {
                                    Console.WriteLine("El producto no cuenta con stock. Lamentamos las molestias");
                                }
                                else
                                {
                                    int compra = 0;
                                    Console.WriteLine("\nEl producto tiene un precio estimado de ${0}." +
                                    "\n¿Desea realizar la compra?" +
                                    "\n1) Si.              2)No.", mother.valor);
                                    compra = int.Parse(Console.ReadLine());
                                    if(compra == 1)
                                    {
                                        prodc_vendido = mother.valor;
                                        Macrohard.añadir_monto(prodc_vendido);
                                        mother.stock -= 1;
                                        Console.WriteLine("Se ha concretado la compra. Muchas gracias por seleccionar nuestro servicio.");
                                    }
                                    else 
                                    {
                                        Console.WriteLine("Lamentamos que el producto no sea de su agrado. Por favor, revise los otros productos.");
                                    }
                                    
                                    
                                }
                                Console.ReadKey();
                                break;
                            default:
                                Console.WriteLine("Ha elegido una opción inválida. Vuelva a intentar con una opción correcta.");
                                Console.ReadKey();
                                break;
                        }

                        break;
                    case 11:
                        Console.Clear();
                        Console.WriteLine("Las ganancias totales de la empresa son: {0}", Macrohard.get_montos);

                        Console.ReadKey();
                        break;
                    case 12:
                        salir = true;
                        Console.ReadKey();
                        break;

                    default:
                        Console.WriteLine("Ha ingresado una opción erronea, por favor vuelva a intentar e ingrese una opción correcta.");
                        Console.ReadKey();
                        break;
                }

            }

        }
    }
}
