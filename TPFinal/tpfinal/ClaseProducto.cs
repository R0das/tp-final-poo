﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpfinal
{
    abstract class ClaseProducto
    {
        protected int precio;
        protected string nombre;
        protected string marca;
        protected int cantidad;

        public string toString()
        {
            return String.Format("Precio: {0}, Nombre: {1}, Marca: {2}, Cantidad: {3}", precio, nombre, marca, cantidad);
        }

        public ClaseProducto(int precio, string nombre, string marca, int cantidad)
        {
            this.precio = precio;
            this.nombre = nombre;
            this.marca = marca;
            this.cantidad = cantidad;
        }
    }
}
