﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpfinal
{
    class ClaseArea
    {

        private ClaseEmpleados jefe_area;
        private string nombre_area;
        private List<ClaseAdministrativo> Admins = new List<ClaseAdministrativo>();
        private List<ClaseProgramadores> Programadores = new List<ClaseProgramadores>();
        private List<ClaseDiseñador> Diseñadores = new List<ClaseDiseñador>();
        private List<ClaseEmpleados> Empleados = new List<ClaseEmpleados>();

        public ClaseArea(ClaseEmpleados jefe, string nombre_area)
        {
            this.nombre_area = nombre_area;
            this.jefe_area = jefe;
        }

        public void contratarEmpleado(string nombre, string apellido, int edad, char sexo, int sueldo, int antiguedad, string tipo_lenguaje, string programa)
        {
            this.Diseñadores.Add(new ClaseDiseñador(nombre, apellido, edad, sexo, sueldo, antiguedad, tipo_lenguaje, programa));
        }
        public void contratarEmpleado(string nombre, string apellido, int edad, char sexo, int sueldo, int antiguedad, bool liderazgo, bool admin_presupuesto)
        {
            this.Admins.Add(new ClaseAdministrativo(nombre, apellido, edad, sexo, sueldo, antiguedad, liderazgo, admin_presupuesto));
        }
        public void contratarEmpleado(string nombre, string apellido, char sexo, int edad, int sueldo, int antiguedad, string lenguaje, string exp_laboral)
        {
            this.Programadores.Add(new ClaseProgramadores(nombre, apellido, sexo, edad, sueldo, antiguedad, lenguaje, exp_laboral));
        }

        public void empleadoTotal(string nombre, string apellido, int edad, char sexo, int sueldo, int antiguedad)
        {
            this.Empleados.Add(new ClaseEmpleados(nombre, apellido, edad, sexo, sueldo, antiguedad));
        }
        public string toString()
        {
            string temp = "";
            temp += String.Format("Nombre de Area: {0}, Jefe de Area: {1} \n Empleados Administrativos: ", nombre_area, jefe_area.toString());
            foreach (ClaseAdministrativo admi in this.Admins)
            {
                temp += admi.toString();
                temp += "\n";
            }
            temp += "Empleados Diseñadores: \n";
            foreach (ClaseDiseñador dise in this.Diseñadores)
            {
                temp += dise.toString();
                temp += "\n";
            }
            temp += "Empleados Programadores: \n";
            foreach (ClaseProgramadores pro in this.Programadores)
            {
                temp += pro.toString();
                temp += "\n";
            }
            return temp;
        }

        public string area_nombre
        {
            get
            {
                return nombre_area;
            }
        }
        public List<ClaseEmpleados> lista_emp
        {
            get
            {
                return this.Empleados;
            }
        }
    }
}
