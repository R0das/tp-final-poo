﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace tpfinal
{
    class ClasePersona
    {

        private string nombre;
        private string apellido;
        private int edad;
        private char sexo;

        public string toString()
        {
            return String.Format("Nombre: {0}, Apellido: {1}, Edad: {2}, Sexo: {3}", nombre, apellido, edad, sexo);
        }

        public ClasePersona(string nombre, string apellido, int edad, char sexo)
        {
            this.nombre = nombre;
            this.apellido = apellido;
            this.edad = edad;
            this.sexo = sexo;
        }
    }
}
