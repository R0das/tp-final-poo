﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpfinal
{
    class ClasePlacadeVideo : ClaseProducto
    {
        private int gb;
        private string salida;

        public string toString()
        {
            return String.Format("{0}, Memoria de la Placa de Video: {1}gb, Tipo de Salida: {2}", base.toString(), gb, salida);
        }

        public ClasePlacadeVideo(int precio, string nombre, string marca, int cantidad, int gb, string salida) : base(precio, nombre, marca, cantidad)
        {
            this.gb = gb;
            this.salida = salida;
        }

        public int stock
        {
            get
            {
                return base.cantidad;

            }
            set
            {
                this.cantidad = value;
            }
        }
        public int valor
        {
            get
            {
                return base.precio;

            }
            set
            {
                this.precio = value;
            }
        }
    }
}
