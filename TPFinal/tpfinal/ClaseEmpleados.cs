﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpfinal
{
    class ClaseEmpleados : ClasePersona
    {

        private int sueldo;
        private int antiguedad;

        public string toString()
        {
            return String.Format("{0}, Sueldo: {1}, Antiguedad: {2}", base.toString(), sueldo, antiguedad);
        }

        public ClaseEmpleados(string nombre, string apellido, int edad, char sexo, int sueldo, int antiguedad) : base(nombre, apellido, edad, sexo)
        {
            this.sueldo = sueldo;
            this.antiguedad = antiguedad;
        }
        public int años_servicio
        {
            get
            {
                return this.antiguedad;
            }
        }
    }
}
