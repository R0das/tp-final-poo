﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpfinal
{
    class ClaseDiseñador : ClaseEmpleados
    {
        private string tipo_lenguaje;
        private string programa;

        public string toString()
        {
            return String.Format("{0}, Tipo de Lenguaje: {1}, Programa: {2}", base.toString(), tipo_lenguaje, programa);
        }

        public ClaseDiseñador(string nombre, string apellido, int edad, char sexo, int sueldo, int antiguedad, string tipo_lenguaje, string programa) : base(nombre, apellido, edad, sexo, sueldo, antiguedad)
        {
            this.tipo_lenguaje = tipo_lenguaje;
            this.programa = programa;
        }
    }
}
