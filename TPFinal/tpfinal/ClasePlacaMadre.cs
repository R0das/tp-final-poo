﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpfinal
{
    class ClasePlacaMadre : ClaseProducto
    {
        private string gen_ram;
        private string chipset;

        public string toString()
        {
            return String.Format("{0}, Generación de RAM: {1}, Chipset: {2}", base.toString(), gen_ram, chipset);
        }

        public ClasePlacaMadre(int precio, string nombre, string marca, int cantidad, string gen_ram, string chipset) : base(precio, nombre, marca, cantidad)
        {
            this.gen_ram = gen_ram;
            this.chipset = chipset;
        }

        public int stock
        {
            get
            {
                return base.cantidad;

            }
            set
            {
                this.cantidad = value;
            }
        }
        public int valor
        {
            get
            {
                return base.precio;

            }
            set
            {
                this.precio = value;
            }
        }
    }
}
