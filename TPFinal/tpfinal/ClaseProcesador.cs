﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpfinal
{
    class ClaseProcesador : ClaseProducto
    {
        private int nucleos;
        private int freq;

        public string toString()
        {
            return String.Format("{0}, Nucleos del Procesador: {1}, Frecuencia del Procesador: {2}", base.toString(), nucleos, freq);
        }

        public ClaseProcesador(int precio, string nombre, string marca, int cantidad, int nucleos, int freq) : base(precio, nombre, marca, cantidad)
        {
            this.nucleos = nucleos;
            this.freq = freq;
        }

        public int stock
        {
            get
            {
                return base.cantidad;

            }
            set
            {
                 this.cantidad = value;
            }
        }
        public int valor
        {
            get
            {
                return base.precio;

            }
            set
            {
                this.precio = value;
            }
        }
    }
}
