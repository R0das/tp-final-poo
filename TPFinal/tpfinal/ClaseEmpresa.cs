﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpfinal
{
    class ClaseEmpresa
    {

        private List<ClaseArea> Areas = new List<ClaseArea>();
        private List<ClaseArea> emp_area = new List<ClaseArea>();
        private string nombre;
        private ClaseProcesador producto_procesadores;
        private ClasePlacadeVideo producto_placadevideo;
        private ClasePlacaMadre producto_placamadre;
        protected int monto_productos_vendidos = 0;


        public ClaseEmpresa(ClaseArea area1, ClaseArea area2, ClaseArea area3, string nombre, ClaseProcesador producto_procesadores, ClasePlacadeVideo producto_placadevideo, ClasePlacaMadre producto_placamadre)
        {
            this.Areas.Add(area1);
            this.Areas.Add(area2);
            this.Areas.Add(area3);
            this.nombre = nombre;
            this.producto_procesadores = producto_procesadores;
            this.producto_placadevideo = producto_placadevideo;
            this.producto_placamadre = producto_placamadre;
        }

        public void añadir_monto(int prodc_vendido)
        {
            monto_productos_vendidos += prodc_vendido;
        }

        public int get_montos
        {
            get
            {
                return monto_productos_vendidos;
            }
        }

        public List<ClaseArea> lista_area
        {
            get
            {
                return this.Areas;
            }
        }

        public void añadirStockCpu(int cpu)
        {
            producto_procesadores.stock += cpu;
        }
        public void añadirStockGpu(int vid)
        {
            producto_placadevideo.stock += vid;
        }

        public void añadirStockMother(int mot)
        {
            producto_placamadre.stock += mot;
        }
    }
}
