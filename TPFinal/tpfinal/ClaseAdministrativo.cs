﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpfinal
{
    class ClaseAdministrativo : ClaseEmpleados
    {
        private bool liderazgo;
        private bool admin_presupuesto;

        public string toString()
        {
            return String.Format("{0}, Liderazgo: {1}, Administración de Presupuesto: {2}", base.toString(), liderazgo, admin_presupuesto);
        }

        public ClaseAdministrativo(string nombre, string apellido, int edad, char sexo, int sueldo, int antiguedad, bool liderazgo, bool admin_presupuesto) : base(nombre, apellido, edad, sexo, sueldo, antiguedad)
        {
            this.liderazgo = liderazgo;
            this.admin_presupuesto = admin_presupuesto;
        }
    }
}
