﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpfinal
{
    class ClaseProgramadores : ClaseEmpleados
    {

        private string lenguaje;
        private string exp_laboral;

        public string toString()
        {
            return String.Format("{0}, Lenguaje: {1}, Experiencia Laboral: {2}", base.toString(), lenguaje, exp_laboral);
        }

        public ClaseProgramadores(string nombre, string apellido, char sexo, int edad, int sueldo, int antiguedad, string lenguaje, string exp_laboral) : base(nombre, apellido, edad, sexo, sueldo, antiguedad)
        {
            this.lenguaje = lenguaje;
            this.exp_laboral = exp_laboral;
        }
    }
}
